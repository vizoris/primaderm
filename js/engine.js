$(function() {


// Фиксированное меню при прокрутке страницы вниз
if ($(window).width() >= '767'){
$(window).scroll(function(){
        var sticky = $('.navbar'),
            scroll = $(window).scrollTop();
        if (scroll > 200) {
            sticky.addClass('navbar-fixed');
        } else {
            sticky.removeClass('navbar-fixed');
        };
    });
}

// BEGIN of script WOW.
new WOW().init();

// Позиционирование блока над картой
var windowWidth = $( window ).width();
var containerWidth = $( ".container" ).width();
$('.map-info').css("left", (windowWidth-containerWidth )/2);

// Позиционирование плашки меню
var windowWidth = $(window).width();
var containerWidth = $(".container").width();
$('.chisel').css("width", (windowWidth - containerWidth) / 2);




// Выпадающее меню
  // BEGIN of script for header submenu
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });


// BEGIN of script for top-slider
var topSlider  =  $('.top-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.top-slider__prev').click(function(){
 	$(topSlider).slick("slickPrev");
});
$('.top-slider__next').click(function(){
 	$(topSlider).slick("slickNext");
 });


// BEGIN of script for welcome-slider
var welcomeSlider  =  $('.welcome-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        responsive: [
        
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
});
 $('.welcome-slider__prev').click(function(){
        $(welcomeSlider).slick("slickPrev");
});
$('.welcome-slider__next').click(function(){
        $(welcomeSlider).slick("slickNext");
 });


// BEGIN of script for staff-slider
var staffSlider  =  $('.staff-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
           
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
});
 $('.staff-slider__prev').click(function(){
        $(staffSlider).slick("slickPrev");
});
$('.staff-slider__next').click(function(){
        $(staffSlider).slick("slickNext");
 });


// BEGIN of script for reviews-slider
var reviewsSlider  =  $('.reviews-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
         responsive: [
        
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
});
 $('.reviews-slider__prev').click(function(){
        $(reviewsSlider).slick("slickPrev");
});
$('.reviews-slider__next').click(function(){
        $(reviewsSlider).slick("slickNext");
 });




// BEGIN of script for documentation-slider
var documentationSlider  =  $('.documentation-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        responsive: [
        
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
});
 $('.documentation-slider__prev').click(function(){
        $(documentationSlider).slick("slickPrev");
});
$('.documentation-slider__next').click(function(){
        $(documentationSlider).slick("slickNext");
 });


// BEGIN of script for certificate-slider
var certificateSlider  =  $('.certificate-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
       
});
 $('.certificate-slider__prev').click(function(){
        $(certificateSlider).slick("slickPrev");
});
$('.certificate-slider__next').click(function(){
        $(certificateSlider).slick("slickNext");
 });


// BEGIN of script for equipment-slider-1
var equipmentslider1  =  $('#equipment-slider-1').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
         responsive: [
        
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
           
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
           
          }
        },
        {
          breakpoint: 761,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
       
});
 $('#equipment-slider__prev1').click(function(){
        $(equipmentslider1).slick("slickPrev");
});
$('#equipment-slider__next1').click(function(){
        $(equipmentslider1).slick("slickNext");
 });



// BEGIN of script for equipment-slider-2
var equipmentslider2  =  $('#equipment-slider-2').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
         responsive: [
        
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
           
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
           
          }
        },
        {
          breakpoint: 761,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
       
});
 $('#equipment-slider__prev2').click(function(){
        $(equipmentslider2).slick("slickPrev");
});
$('#equipment-slider__next2').click(function(){
        $(equipmentslider2).slick("slickNext");
 });




// BEGIN of script for offers-slider
var offersSlider  =  $('.offers-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        responsive: [
        
        
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
           
          }
        },
        {
          breakpoint: 761,
          settings: {
            slidesToShow: 1,
           
          }
        }
      ]
       
});
 $('.offers-slider__prev').click(function(){
        $(offersSlider).slick("slickPrev");
});
$('.offers-slider__next').click(function(){
        $(offersSlider).slick("slickNext");
 });




// Карта
var myMap;
    ymaps.ready(function () {
        myMap = new ymaps.Map("YMapsID", {
            center: [51.772931, 36.177582],
            zoom: 16
        });
        
        myMap.controls.add('smallZoomControl');
        myMap.controls.add(new ymaps.control.ScaleLine());
        
    var myPlacemark = new ymaps.Placemark([51.772931, 36.177582], {
    hintContent: '1-я Владимирская улица, 10Бс3',
    
},{
   
        iconLayout: 'default#image',
        iconImageHref: '../img/marker.png',
    });
        myMap.geoObjects.add(myPlacemark);
      
        });



})